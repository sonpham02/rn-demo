import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    TouchableNativeFeedback,
    Image,
    Platform,
    SegmentedControlIOS,
    ScrollView
} from 'react-native';

var NavigatorNavigationBarStylesIOSCustom = require('./NavigatorNavigationBarStylesIOSCustom');
var NavigatorNavigationBarStyles = Platform.OS === 'android' ?
    null : NavigatorNavigationBarStylesIOSCustom;
var SCREEN_HEIGHT = require('Dimensions').get('window').height;

var MockUpText = React.createClass({
    render: function () {
        return(
            <View style={{marginTop: 10}}>
                <View style={{marginTop: 10, marginBottom: 10}}>
                    <Text>Lorem Ipsum</Text>
                    <Text>Lorem Ipsum</Text>
                    <Text>99999999</Text>
                </View>

                <ScrollView style={styles.scrollView}>
                    <Text>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                    </Text>
                </ScrollView>
                <View style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: 1.5}}/>
                <View>
                    <Text>Lorem Ipsum</Text>
                </View>
            </View>
        )
    }
});

var SlideScene = React.createClass({

    getInitialState() {
        return {
            values: ['Detail', 'Testimonial'],
            selectedIndex: 0
        }
    },

    _handlePress() {
        this.props.navigator.pop();
    },

    render() {
        return (
            <View style={[styles.container,{backgroundColor: 'white'}]}>
                <View style={{margin: 10}}>
                    <SegmentedControlIOS
                        values={this.state.values}
                        selectedIndex={this.state.selectedIndex}
                    />
                    <MockUpText/>
                </View>
            </View>
        )
    }
});

const styles = StyleSheet.create({

    container: {
        flex: 1,
        marginTop:   NavigatorNavigationBarStyles.General.TotalNavHeight
    },
    scrollView: {
        height: 0.3 * SCREEN_HEIGHT,
        marginBottom: 15
    }
});

module.exports = SlideScene;