import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    TouchableNativeFeedback,
    Image,
    Platform,
} from 'react-native';

var Swiper = require('react-native-swiper');
var NavigatorNavigationBarStylesIOSCustom = require('./NavigatorNavigationBarStylesIOSCustom');
var NavigatorNavigationBarStyles = Platform.OS === 'android' ?
    null : NavigatorNavigationBarStylesIOSCustom;
var SCREEN_HEIGHT = require('Dimensions').get('window').height;

const dot = <View style={{
                backgroundColor:'rgba(0,0,0,.2)',
                width: 8,
                height: 8,
                borderRadius: 4,
                margin: 3
             }}/>;

const activeDot = <View style={{
                            backgroundColor: '#000',
                            width: 8, height: 8,
                            borderRadius: 4,
                            margin: 3}}/>;


var SwipeScene = React.createClass({
    _handlePress() {
        this.props.navigator.push({id: 2, title: 'Detail'});
    },

    render() {
        return (
            <View style={{flex: 1}}>

                <View style={styles.container}>
                    <Swiper style={styles.wrapper}
                            height={SCREEN_HEIGHT - NavigatorNavigationBarStyles.General.TotalNavHeight}
                            horizontal={false}
                            dot={dot}
                            activeDot={activeDot}
                            paginationStyle={styles.paginationStyle}>
                        <View style={styles.slide}>
                            <Image
                                source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}}
                                style={[styles.image,{backgroundColor: 'rgba(0, 0, 100, 0.25)'}]}
                                resizeMode={Image.resizeMode.contain}
                            />
                        </View>
                        <View style={styles.slide2}>
                            <Text style={styles.text}>Beautiful</Text>
                        </View>
                        <View style={styles.slide3}>
                            <Text style={styles.text}>And simple</Text>
                        </View>
                    </Swiper>
                </View>

                <View style={styles.sideBar}>
                    <TouchableOpacity onPress={this._handlePress}>
                        <View style={{paddingVertical: 5, paddingHorizontal: 5, backgroundColor: 'black'}}>
                            <Text style={{color: 'white'}}>Detail</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'center',
        marginTop:   NavigatorNavigationBarStyles.General.TotalNavHeight,
        //backgroundColor: '#F5FCFF',
        position: 'relative'
    },
    wrapper: {

    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    image: {
        flex: 1,
        width: 400,
        height: 400
    },
    slide: {
        flex: 1,
        alignItems: 'center'
    },
    paginationStyle: {
        left: 10,
        right: null
    },
    sideBar: {
        flex: 1,
        position: 'absolute',
        top: 64,
        right: 0
    }
});

module.exports = SwipeScene;