import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Navigator,
    View,
    TouchableOpacity,
    TouchableHighlight,
    TouchableNativeFeedback,
    Image,
    Platform,
} from 'react-native';


var NavigatorNavigationBarStylesIOSCustom = require('./app/NavigatorNavigationBarStylesIOSCustom');
var NavigatorNavigationBarStyles = Platform.OS === 'android' ? null : NavigatorNavigationBarStylesIOSCustom;
var Icon = require('react-native-vector-icons/Ionicons');

var SwipeScene = require('./app/SwipeScene');
var SlideScene = require('./app/SlideScene');
var TransitionConfig = require('./app/TransitionCustomConfig');

var BaseConfig = Navigator.SceneConfigs.VerticalDownSwipeJump;
var CustomSceneConfig = Object.assign({}, BaseConfig, TransitionConfig.CustomSceneConfig);

class Example extends Component {
  _renderScene(route, navigator) {
    if (route.id === 1) {
      return <SwipeScene navigator={navigator} />
    } else if (route.id === 2) {
      return <SlideScene navigator={navigator} />
    }
  }

  _configureScene(route) {
    return CustomSceneConfig;
  }

  render() {
    return (
        <Navigator
            initialRoute={{
                id: 1,
                title: 'Zalora'
            }}
            renderScene={this._renderScene}
            configureScene={this._configureScene}
            navigationBar={
                <Navigator.NavigationBar style={{backgroundColor: '#000'}}
                                         routeMapper={NavigationBarRouteMapper}
                                         navigationStyles={NavigatorNavigationBarStyles}/>
            }
    />
  );
  }
}

var NavigationBarRouteMapper = {
    LeftButton(route, navigator, index, navState) {
        if(index == 0) {
            return null;
        }
        const previousRoute = navState.routeStack[index - 1];
        return (
            <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}
                              onPress={() => navigator.pop()}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Icon name="ios-arrow-back" size={25} color={'#ffffff'} style={{marginLeft: 3, marginTop: 2}}/>
                    <Text style={{color: 'white', marginLeft: 4}}>
                        {previousRoute.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    },
    RightButton(route, navigator, index, navState) {
        if(route.rightElement) {
            return route.rightElement;
        }
    },
    Title(route, navigator, index, navState) {
        return (
                <Text style={{color: 'white', margin: 10, fontSize: 16}}>
                    {route.title}
                </Text>
        );
    }
};

const styles = StyleSheet.create({
});

AppRegistry.registerComponent('Example', () => Example);
